#ifndef LISTA_H
#define LISTA_H

//struct uczen;


#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <conio.h>
#include "uczen.h"

using namespace std;

class lista
{

    private:

    uczen *pierwszy; // deklaruje wskaźnik na poczatek listy

    public:

    void dodaj_ucznia(string imie, string nazwisko);
    void usun_ucznia(int numer);
    void wyswietl_liste();
    void wyswietl_ucznia(int numer);
    void save();
    void load();
    lista();

};


#endif // LISTA_H

