#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <conio.h>
#include "uczen.h"
#include "lista.h"

int main()
{

    int wybor;
    string n;
    string sr;
    int namber = 1;
    int program_on=0;

    lista *dziennik = new lista;

    do
    {

        cout<<endl<<endl<<"Program ten tworzy prosta liste uczniow."<<endl;
        cout<<"Wybierz (prosze) co chcesz zrobic: "<<endl<<endl;
        cout<<"1 - dodanie nowego ucznia do listy."<<endl;
        cout<<"2 - usuniecie ucznia z listy."<<endl;
        cout<<"3 - wyswietlenie listy."<<endl;
        cout<<"4 - wyswietlenie konkretnego ucznia"<<endl;
        cout<<"5 - zakonczenie programu"<<endl;
        cout<<"6 - zapisanie listy do pliku"<<endl;
        cout<<"7 - zaladowanie listy z pliku"<<endl;
        cin>>wybor;

        switch(wybor)
        {
        case 1:


            cout<<endl<<"Podaj prosze imie ucznia"<<endl;
            cin>>n;
            cout<<endl<<"Podaj prosze nazwisko ucznia"<<endl;
            cin>>sr;

            dziennik->dodaj_ucznia(n, sr);
            namber++;
            break;

        case 2:
            int nr_del;
            cout<<endl<<endl<<"Oto lista uczniow dla przypomnienia: "<<endl;
            dziennik->wyswietl_liste();
            cout<<endl<<endl<<"Wybierz numer ucznia do usuniecia: "<<endl;
            cin>>nr_del;
            dziennik->usun_ucznia(nr_del);
            break;

        case 3:
            dziennik->wyswietl_liste();
            cout << endl << "Wcisnij dowolny klawisz aby kontynuowac" << endl;
            getch();
            break;

        case 4:
            int numer;
            cout<<endl<<endl<<"Podaj prosze numer ucznia: "<<endl;
            cin>>numer;
            dziennik->wyswietl_ucznia(numer);
            cout << endl << "Wcisnij dowolny klawisz aby kontynuowac" << endl;
            getch();
            break;

        case 5:
            cout<<endl<<"Czy chcesz zakonczyc dzialanie programu?"<<endl;
            cout<<"Tak - wcisnij 1."<<endl;
            cout<<"Nie - wcisnij 0."<<endl;
            cin>>program_on;
            break;

        case 6:

            dziennik->save();
            cout << endl << "zapisano" << endl;
            cout << endl << "Wcisnij dowolny klawisz aby kontynuowac" << endl;
            getch();
            break;


        case 7:

            cout << endl << "Wyczytwanie danych." << endl;
            dziennik->load();
            cout << endl << "Wcisnij dowolny klawisz aby kontynuowac" << endl;
            getch();
            break;


        default:
            cout<<endl<<"Musisz wybrac liczbe pomiedzy 1 a 5"<<endl;
            cout << endl << "Wcisnij dowolny klawisz aby kontynuowac" << endl;
            getch();
        }


        system("cls");

    }while(program_on==0);



    delete (dziennik);
    return 0;
}

