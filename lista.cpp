#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <conio.h>
#include "lista.h"

using namespace std;

lista::lista()
{
    pierwszy=0; //konstruktor wskaźnika??
}

void lista::dodaj_ucznia(string imie, string nazwisko)
{
    uczen *nowy= new uczen; //tworzymy nowy element listy

    nowy->imie=imie;
    nowy->nazwisko=nazwisko;

    if(pierwszy==0)  //jesli ten nowo dodany element to pierwszy element listy
    {
        //to wskaźnik nowego elementu jest poczatkiem listy
        pierwszy=nowy;
    }

    else  //jeśli nie:
    {
        uczen *temp = pierwszy;
        while(temp->nastepny)  // w cudowny sposób znajdujemy wskaźnik na ostatni element
        {
            temp=temp->nastepny;
        }
        temp->nastepny=nowy; //
    }
}


void lista::wyswietl_liste()
{
    int nr=1;
    uczen *temp = pierwszy;

    while(temp)
    {

        cout<<"imie ucznia: "<<temp->imie<<", Nazwisko: "<<temp->nazwisko<<", numer ucznia: "<<nr<<endl;
        temp=temp->nastepny;
        nr++;
    }
}

void lista::wyswietl_ucznia(int numer)
{
    if( numer >= 1 )
    {
        uczen *temp = pierwszy;
        for( int j = 1; temp != 0; ++j, temp = temp->nastepny)
        {
            if( j == numer )
            {
                cout<<"Imie ucznia: "<<temp->imie<<", Nazwisko: "<<temp->nazwisko<<endl;
                break;
            }
        }

        if( 0 == temp )
        {
            cout<<endl<<"Wybrano zla liczbe"<<endl;
        }
    }
    else
    {
        cout << endl << "Wybrano zla liczbe" << endl;
    }
}


void lista::usun_ucznia(int numer)
{
    // robimy licznik kontrolny listy, metoda młota.
    uczen *temp = pierwszy;
    int nr = 0;

    while(temp)
    {
        temp = temp -> nastepny;
        nr++;
    }

    if(numer==1 && numer <= nr)
    {
        uczen *del;
        uczen *temp=pierwszy;
        del=temp;
        pierwszy=temp->nastepny;
        delete del;

    }
    else if(numer>=2 && numer <= nr)
    {
        int j=1;

        uczen *temp=pierwszy;
        uczen *del;

        while(temp)
        {
            if((j+1)==numer)break;
            temp=temp->nastepny;
            j++;
        }

        if(temp->nastepny->nastepny==0)
        {
            del = temp->nastepny;
            temp->nastepny=0;
            delete del;
        }
        else
        {
            del=temp->nastepny;
            temp->nastepny=temp->nastepny->nastepny;
            delete del;
        }
    }
    else
    {
        cout << endl << "Wybrano zla liczbe." << endl;
        cout << endl << "Wcisnij dowolny klawisz aby kontynuowac" << endl;
        getch();
    }
}


void lista::save()
{

    uczen *temp = pierwszy;
    ofstream outputFile("Lista1.bin", ios::out | ios::binary);

    while(temp)
    {
        int size1 = (temp->imie.size());
        outputFile.write(reinterpret_cast<char*>(&size1), sizeof(int));
        outputFile.write(temp->imie.c_str(), size1);

        int size2 = (temp->nazwisko.size());
        outputFile.write(reinterpret_cast<char*>(&size2), sizeof(int));
        outputFile.write(temp->nazwisko.c_str(), size2);

        temp=temp->nastepny;
    }

    outputFile.close();
}

void lista::load()
{
    int usun=0;
    char* inputBuffer;
    ifstream inputFile("Lista1.bin", ios::in | ios::binary);

        while (!inputFile.eof())
        {
            int size1=0;
            string str1;

            inputFile.read(reinterpret_cast<char *>(&size1), sizeof(int));
            inputBuffer = new char[size1];
            inputFile.read(inputBuffer, size1);
            str1 = "";
            str1.append(inputBuffer, size1);

            delete[] inputBuffer;

            int size2=0;
            string str2;

            inputFile.read(reinterpret_cast<char*>(&size2), sizeof(int));
            inputBuffer = new char[size2];
            inputFile.read(inputBuffer, size2);
            str2 = "";
            str2.append(inputBuffer, size2);
            delete[] inputBuffer;

            dodaj_ucznia(str1, str2);
            usun++;
        }

    // robimy to na młota, trzeba się jeszcze nauczyć jak omijać ostatni znak pliku.
    usun_ucznia(usun);
    inputFile.close();

}
