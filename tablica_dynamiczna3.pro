TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    uczen.cpp \
    lista.cpp

HEADERS += \
    uczen.h \
    lista.h
